package ru.sper.spring.Java13SpringSU.libraly.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.sper.spring.Java13SpringSU.libraly.model.Genre;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String filmTitle;
    private String directorsFio;
    private Genre genre;
}
