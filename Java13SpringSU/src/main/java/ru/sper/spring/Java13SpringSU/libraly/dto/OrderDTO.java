package ru.sper.spring.Java13SpringSU.libraly.dto;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderDTO extends GenericDTO {
    private UserDTO user;
    private FilmDTO filmDTO;
    private LocalDate rentDate;
    private Integer rentPeriod;
    private Boolean isPurchased;
    private Boolean isBuyed;
    private Long filmId;
    private Long userId;
}



