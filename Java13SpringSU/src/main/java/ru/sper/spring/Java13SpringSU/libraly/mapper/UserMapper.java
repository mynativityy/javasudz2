package ru.sper.spring.Java13SpringSU.libraly.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sper.spring.Java13SpringSU.libraly.dto.UserDTO;
import ru.sper.spring.Java13SpringSU.libraly.model.GenericModel;
import ru.sper.spring.Java13SpringSU.libraly.model.User;
import ru.sper.spring.Java13SpringSU.libraly.repository.OrderRepository;
import ru.sper.spring.Java13SpringSU.libraly.utils.DateFormatter;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Collectors;
import java.time.LocalDate;


@Component
public class UserMapper extends GenericMapper<User, UserDTO> {

    private final ModelMapper modelMapper;
    private final OrderRepository orderRepository;

    protected UserMapper(ModelMapper modelMapper, OrderRepository orderRepository) {
        super(modelMapper, User.class, UserDTO.class);
        this.modelMapper = modelMapper;
        this.orderRepository = orderRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrdersIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setBirthDate)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setCreatedWhen)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getOrdersIds())) {
            destination.setOrders(new HashSet<>(orderRepository.findAllById(source.getOrdersIds())));
        }
        else {
            destination.setOrders(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
        destination.setCreatedWhen(LocalDate.now());
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrdersIds(Objects.isNull(source) || Objects.isNull(source.getOrders()) ? null
                : source.getOrders()
                .stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet()));
    }
}
