package ru.sper.spring.Java13SpringSU.libraly.mapper;

import ru.sper.spring.Java13SpringSU.libraly.dto.GenericDTO;
import ru.sper.spring.Java13SpringSU.libraly.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtoList);

    List<D> toDTOs(List<E> entitiesList);
}
