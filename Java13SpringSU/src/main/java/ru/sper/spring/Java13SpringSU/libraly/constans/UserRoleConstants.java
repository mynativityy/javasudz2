package ru.sper.spring.Java13SpringSU.libraly.constans;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String LIBRARIAN = "LIBRARIAN";
    String USER = "USER";
}
