package ru.sper.spring.Java13SpringSU.libraly.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IdFilmDirectorDTO {
    private Long filmId;
    private Long directorId;
}
