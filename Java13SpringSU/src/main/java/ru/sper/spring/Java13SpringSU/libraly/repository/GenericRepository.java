package ru.sper.spring.Java13SpringSU.libraly.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Component;
import ru.sper.spring.Java13SpringSU.libraly.model.GenericModel;

import java.util.List;

@NoRepositoryBean
public interface GenericRepository<T extends GenericModel> extends JpaRepository<T, Long> {

    Page<T> findAllByIsDeletedFalse(Pageable pageable);

    List<T> findAllByIsDeletedFalse();
}
