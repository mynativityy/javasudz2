package ru.sper.spring.Java13SpringSU.libraly.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import ru.sper.spring.Java13SpringSU.libraly.model.Order;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
    Page<Order> getOrderByUserId(Long userId, Pageable pageable);
}
