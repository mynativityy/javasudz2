package ru.sper.spring.Java13SpringSU.libraly.Restcontroller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sper.spring.Java13SpringSU.libraly.dto.DirectorDTO;
import ru.sper.spring.Java13SpringSU.libraly.dto.FilmDTO;
import ru.sper.spring.Java13SpringSU.libraly.model.Director;
import ru.sper.spring.Java13SpringSU.libraly.service.DirectorService;
import ru.sper.spring.Java13SpringSU.libraly.service.FilmService;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссёры", description = "Контроллер для работы с режиссёрами фильмов")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController extends GenericController<Director, DirectorDTO> {

    private final DirectorService directorService;
    private final FilmService filmService;

    public DirectorController(DirectorService directorService, FilmService filmService) {
        super(directorService);
        this.directorService = directorService;
        this.filmService = filmService;
    }

    @Operation(description = "Добавить фильм к режиссёру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                               @RequestParam(value = "directorId") Long directorId) {
        DirectorDTO directorDTO = directorService.getOne(directorId);
        FilmDTO filmDTO = filmService.getOne(filmId);
        directorDTO.getFilmsIds().add(filmDTO.getId());
        return ResponseEntity.status(HttpStatus.OK).body(directorService.update(directorDTO));
    }
}
