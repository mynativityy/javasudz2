package ru.sper.spring.Java13SpringSU.libraly.dto;

import lombok.*;
import ru.sper.spring.Java13SpringSU.libraly.model.Director;
import ru.sper.spring.Java13SpringSU.libraly.model.Film;
import ru.sper.spring.Java13SpringSU.libraly.model.Genre;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilmDTO extends GenericDTO {
    private String filmTitle;
    private Integer premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
    private Set<Long> ordersIds;
    private boolean isDeleted;

    private String link;

    public FilmDTO(Film film) {
        FilmDTO filmDTO = new FilmDTO();
        filmDTO.setFilmTitle(film.getFilmTitle());
        filmDTO.setPremierYear(film.getPremierYear());
        filmDTO.setCountry(film.getCountry());
        filmDTO.setGenre(film.getGenre());
        filmDTO.setLink(film.getLink());
        Set<Director> directors = film.getDirectors();
        Set<Long> directorIds = new HashSet<>();
        if (directors != null && directors.size() > 0) {
            directors.forEach(a -> directorIds.add(a.getId()));
        }
        filmDTO.setDirectorsIds(directorIds);
    }
}

