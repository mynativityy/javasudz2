package ru.sper.spring.Java13SpringSU.libraly.model;

public enum Genre {
    FANTASY("Фантастика"),
    COMEDY("Комедия"),
    DRAMA("Драма");

    private final String genreTextDisplay;

    Genre(String genreName) {
        this.genreTextDisplay = genreName;
    }

    public String getGenreTextDisplay() {
        return this.genreTextDisplay;
    }
}
