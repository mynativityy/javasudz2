package ru.sper.spring.Java13SpringSU.libraly.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.sper.spring.Java13SpringSU.libraly.dto.FilmDTO;
import ru.sper.spring.Java13SpringSU.libraly.dto.OrderDTO;
import ru.sper.spring.Java13SpringSU.libraly.mapper.OrderMapper;
import ru.sper.spring.Java13SpringSU.libraly.model.Order;
import ru.sper.spring.Java13SpringSU.libraly.repository.OrderRepository;

import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class OrderService extends GenericService<Order, OrderDTO> {
    private final FilmService filmService;
    private final OrderMapper orderMapper;
    private final OrderRepository orderRepository;

    protected OrderService(OrderRepository orderRepository, OrderMapper orderMapper, FilmService filmService) {
        super(orderRepository, orderMapper);
        this.filmService = filmService;
        this.orderMapper = orderMapper;
        this.orderRepository = orderRepository;
    }

    public Page<OrderDTO> listUserRentFilms(final Long id, final Pageable pageable) {
        Page<Order> objects = orderRepository.getOrderByUserId(id, pageable);
        List<OrderDTO> results = orderMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public OrderDTO rentFilm(OrderDTO orderDTO) {
        FilmDTO filmDTO = filmService.getOne(orderDTO.getFilmId());
        long rentPeriod = orderDTO.getRentPeriod() != null ? orderDTO.getRentPeriod() : 14L;
        orderDTO.setIsBuyed(false);
        orderDTO.setRentDate(LocalDate.now());
        orderDTO.setRentPeriod((int) rentPeriod);
        orderDTO.setIsPurchased(false);
        return orderMapper.toDTO(repository.save(orderMapper.toEntity(orderDTO)));
    }
    public OrderDTO buyFilm(OrderDTO orderDTO) {
        log.info("Я ЗАШЕЛ");
        orderDTO.setIsBuyed(true);
        FilmDTO filmDTO = filmService.getOne(orderDTO.getFilmId());
        long rentPeriod = 0;
        orderDTO.setRentDate(LocalDate.now());
        orderDTO.setRentPeriod((int) rentPeriod);
        orderDTO.setIsPurchased(false);
        return orderMapper.toDTO(repository.save(orderMapper.toEntity(orderDTO)));
    }
}
