package ru.sper.spring.Java13SpringSU.libraly.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
