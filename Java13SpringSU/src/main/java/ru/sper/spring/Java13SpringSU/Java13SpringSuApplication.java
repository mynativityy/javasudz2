package ru.sper.spring.Java13SpringSU;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java13SpringSuApplication {

	public static void main(String[] args) {
		SpringApplication.run(Java13SpringSuApplication.class, args);
	}

}
